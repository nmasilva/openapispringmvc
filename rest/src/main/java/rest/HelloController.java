package rest;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/greeting", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class HelloController {
	private static final Logger logger = LoggerFactory.getLogger(HelloController.class);
	
	@GetMapping("/")
	public @ResponseBody ResponseEntity<String> getGreeting() {
		logger.info("Greeting endpoing call received on: " + LocalDate.now());
		
		return new ResponseEntity<>("Hello World!", HttpStatus.OK);
	}
}
